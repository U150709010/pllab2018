use strict;
use warnings;

print "Hello, this is my first script\n" ;

my $scalar_variable = 5;
print 'the value of $scalar_variable is ' . "$scalar_variable" . "\n";

$scalar_variable = "this is now string" ;
print 'the value of $scalar_variable now is ' . "$scalar_variable" . "\n";

my @a = (10,20,30);
print "@a\n";

@a = (10,"this is string", 5.9, $scalar_variable);
print "@a\n";

