The size of bool is:1 byte
The size of char is:1 byte
The size of int is:4 byte
The size of arr is:40 byte 
The size of the first element arr is pointing to:4 bytes
The size of double is:8 bytes

The address of the bool is:140731944587767
The address of the char is:140731944587766
The address of the int is:140731944587768
The address of the arr is:140731944587824
The address of the arr[1] is:140731944587828
The address of the arr[0] is:140731944587824
The address of the double is:140731944587776
Can you type the address of arr[2] and press enter >>140731944587832
YES! You guessed right! 

